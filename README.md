# ccSAG2

ccSAG2 is a software that divides single-cell sequence assemblies into same strain groups and that cleans the chimeric sequencing reads by comparison sequencing reads with sequencing assemblies of same strains. After running ccSAG2, a composite single-cell genome can be constructed by co-assembling of cleaned-up sequencing reads.

# Features

* Clustering single-cell sequence assemblies into same bacterial strains
* Cleaning chimeric sequencing reads by comparison sequence reads with sequencing assemblies of same strains
* Assembling composite single-cell genomes by co-assembly of cleaned sequencing reads

# Requirements

The following tools should be installed:

* blastn (>= 2.9.0+)
* fastANI (>= 1.3)
* jellyfish (>= 2.3.0)
* minimap2 (>= 2.17)
* python (>= 3.7.6)
* samtools (>= 1.9)
* seqkit (>= 0.12.1)

This tool requires the following python libraries:

* joblib
* pysam
* pandas
* tqdm
* scikit-learn
* kneed

# Installation

Download the packages from https://gitlab.com/bitbiome-oss/ccsag2 and grant permission to run ccSAG2.

```bash
$ wget https://gitlab.com/bitbiome-oss/ccsag2/-/archive/vX.X.X/ccsag2-vX.X.X.tar.gz
$ tar zxvf ccsag2-vX.X.X.tar.gz
$ cd ccsag2-vX.X.X
$ chmod a+x ccsag2.py
```

# Usage

### Clustering-fast

Clustering single-cell sequence assemblies into same bacterial strains by DBSCAN, SetCover, or Star clustering.
Input files are nucleotide fasta files of the prediction transcripts.

#### DBSCAN clustering

```bash
$ ccsag2.py clustering-fast -o ccsag -w ccsag/work DBSCAN sag1.ffn sag2.ffn sag3.ffn sag4.ffn sag5.ffn
```

#### SetCover clustering

```bash
$ ccsag2.py clustering-fast -o ccsag -w ccsag/work SetCover sag1.ffn sag2.ffn sag3.ffn sag4.ffn sag5.ffn
```

#### Star clustering

```bash
$ ccsag2.py clustering-fast -o ccsag -w ccsag/work Star sag1.ffn sag2.ffn sag3.ffn sag4.ffn sag5.ffn
```

### Clustering-careful

Clustering single-cell sequence assemblies into same bacterial strains carefully by ANI, TNF, and similarity of single copy marker genes.
Input files are nucleotide fasta files of the contig sequences.

```bash
$ ccsag2.py clustering-carefull -o ccsag -w work sag1.fna sag2.fna sag3.fna sag4.fna sag5.fna
```

### Cleaning

Cleaning chimeric sequencing reads by comparison sequence reads with sequencing assemblies of same strains.
Fastq of SAG1 is mapped to SAG2, SAG3, ..., SAGN assemblies which are the same strain as SAG1 by Minimap2 and the detected chimeric sequencing reads are fragmented as appropriate reads. 
Input files are a fastq file you want to clean and nucleotide fasta files of the contig sequences.

#### Single-end

```bash
$ ccsag2.py cleaning -o ccsag -w work sag1.fastq.gz sag2.fasta.gz sag3.fasta.gz
```

#### Paired-end

```bash
$ ccsag2.py cleaning -o ccsag -w work -p sag1_2.fastq.gz sag1_1.fastq.gz sag2.fasta.gz sag3.fasta.gz
```

# Output

|Command|File|
|----|----|
|clustering-fast DBSCAN|DBSCAN_result.json|
|clustering-fast SetCover|SetCover_result.json|
|clustering-fast Star|Star_result.json|
|clustering-careful|clustering_result.txt|
|cleaning|*_cleaned.fastq.gz|

# Constructing composite SAG

```bash
$ cat SAG1_cleaned.fastq.gz SAG2_cleaned.fastq.gz SAG3_cleaned.fastq.gz > COSAG_cleaned.fastq.gz
$ spades.py --sc -k 21,33,55,77,99,127 --disable-rr --disable-gzip-output -s COSAG_cleaned.fastq.gz -o output_spades
```

# License

ccSAG2 is licensed under the  LGPL-2.1 License.

# References

* Kogawa, Masato, et al. ["Obtaining high-quality draft genomes from uncultured microbes by cleaning and co-assembly of single-cell amplified genomes."](https://www.nature.com/articles/s41598-018-20384-3) Scientific reports 8.1 (2018): 2059.

