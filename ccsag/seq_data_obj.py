import gzip
import logging
import os
import pysam

logger = logging.getLogger()

class Fasta:
    def __init__(self, file_path):
        basename = os.path.basename(file_path)
        self.name = os.path.splitext(basename)[0]
        self.path = file_path
        logging.debug(f"class Fasta was created: '{self.name}'")
    
    def getSequences(self):
        # get list of gene seq only
        with open(self.path) as f:
            text = f.readlines()
        seq_list = []
        for line in text:
            if line[0:1] == ">":
                seq_list.append("")
            else:
                seq_list[-1] += line.rstrip().upper()
        return seq_list


class Fastq:
    def __init__(self, forward, reverse=None):
        self.name = os.path.basename(forward).replace(".fastq.gz","")
        self.forward = forward
        if reverse != None:
            self.reverse = reverse
            self.isPaired = True
        else:
            self.reverse = None
            self.isPaired = False
        logging.debug(f"class Fastq has been created: '{self.name}'")


class Bam:
    def __init__(self, bam, reverse=None):
        self.bam = bam
        if reverse != None:
            self.reverse = reverse
            self.isPaired = True
        else:
            self.reverse = None
            self.isPaired = False
        logging.debug(f"class Bam has been created")
    
    def rmChimera(self, ofastq, min_len=21):
        logging.info(f"Chimeric reads are being removed: {ofastq}")
        with gzip.open(ofastq, 'wt') as fo, pysam.AlignmentFile(self.bam, 'rb') as fh:
            for read in fh:
                read_num = 1 if read.is_read1 else 2
                for i, seq, qual in Bam.Fragment.each(read):
                    if len(seq) < min_len:
                        continue
                    fo.write('@{}:{}:{}\n'.format(read.query_name, read_num, i))
                    fo.write('{}\n'.format(seq))
                    fo.write('+\n')
                    fo.write('{}\n'.format(qual))
    
    class Fragment:
        def __init__(self, pos=0):
            self.start = pos
            self.end = pos + 1

        def expand(self, length):
            self.end += length - 1

        @classmethod
        def each(self, read):
            if read.cigartuples is None:
                yield(0, read.query_sequence, ''.join(list(map(lambda x: chr(x + 33), read.query_qualities))))
            else:
                # 0:M:BAM_CMATCH, 1:I:BAM_CINS, 3:N:BAM_CREF_SKIP, 4:S:BAM_CSOFT_CLIP
                frags = []
                frag = None
                for t in read.cigartuples:
                    if t[0] in (0, 3, 4):
                        frag = Bam.Fragment() if frag is None else Bam.Fragment(frag.end)
                        frags.append(frag)
                    if t[0] in (0, 1, 4):
                        frag.expand(t[1])
                for i, frag in enumerate(frags):
                    subseq = read.query_sequence[frag.start:frag.end]
                    subqual = read.query_qualities[frag.start:frag.end]
                    subqual_str = ''.join(list(map(lambda x: chr(x + 33), subqual)))
                    yield(i, subseq, subqual_str)