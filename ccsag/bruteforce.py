import itertools
import logging
import os
import pandas as pd
import shutil
import time
import os
import tqdm

logger = logging.getLogger()

class BruteForce:
    def __init__(self, *fasta):
        self.fasta = list(fasta)
        self.dist = {}
        self.wdir = f"{os.getcwd()}/work"
        logging.debug("class BruteForce had been initialized.")
    
    # calculate SAG-SAG distance
    def calcDist(self):
        # method will be overrided in child class
        logging.debug("calcDist method is undefined in class BruteForce.")
        
    # return calculated distances
    def getDist(self):
        return self.dist
    
    def setWorkingDir(self, dir_name):
        self.wdir = dir_name
        logging.debug(f"Working directory had been set. {self.wdir}")
    
    def clean(self):
        shutil.rmtree(self.wdir)
        logging.debug(f"Working directory had been removed. {self.wdir}")


class Jaccard(BruteForce):
    def __init__(self, *fasta):
        super().__init__(*fasta)
        self.wdir = f"{self.wdir}/Jaccard"
        logging.debug("class Jaccard had been initialized.")
    
    def calcDist(self):
        logging.info("Calculation of SAG-SAG distance started.")
        
        os.makedirs(self.wdir, exist_ok=True)
        
        # calculate SAG-SAG distance toward all SAG pairs
        SAG_pairs = itertools.combinations_with_replacement(self.fasta, 2)
        result_txt = ["query\tsubject\tdist"]
        for SAG_a, SAG_b in tqdm.tqdm(list(SAG_pairs), desc="calc_SAG_dist"):
            
            # calculate distance
            SAG_a_seq = SAG_a.getSequences()
            SAG_b_seq = SAG_b.getSequences()
            if SAG_a != SAG_b:
                dist = self.__calc_jaccard(SAG_a_seq, SAG_b_seq)
            else:
                dist = 1
            
            if SAG_a not in self.dist:
                self.dist[SAG_a] = {}
            if SAG_b not in self.dist:
                self.dist[SAG_b] = {}
            self.dist[SAG_a][SAG_b] = dist
            self.dist[SAG_b][SAG_a] = dist
            
            result_txt.append(f"{SAG_a.name}\t{SAG_b.name}\t{str(dist)}")
            logging.info(f"SAG-SAG distance calculated: {SAG_a.name}, {SAG_b.name}, dist={str(dist)}")
        
        # output result
        otxt = self.wdir + "/JaccardDistance.txt"
        with open(otxt, "w") as o:
            o.write("\n".join(result_txt))
        logging.info(f"SAG distance matrix was saved as '{otxt}'")
        df = pd.DataFrame(self.dist).fillna(0)
        df.index = [SAG.name for SAG in df.index.tolist()]
        df.columns = [SAG.name for SAG in df.columns.tolist()]
        otsv = self.wdir + "/JaccardDistance.tsv"
        df.to_csv(otsv, sep="\t")
        logging.info(f"SAG distance matrix was saved as '{otsv}'")
        
        logging.info("Calculation of SAG-SAG distance finished.")
        
        
    def __calc_jaccard(self, seq_list_a, seq_list_b):
        """
        SAG-SAG distance:
            (number of identical gene with compared SAG) / (total gene num of small SAG)
        """
        start = time.time()
        
        # smaller SAG = query, larger SAG = subject
        if len(seq_list_a) <= len(seq_list_b):
            query, subject = set(seq_list_a), set(seq_list_b)
        else:
            query, subject = set(seq_list_b), set(seq_list_a)
            
        # calculate SAG-SAG distance based on number of identical gene
        query_unique = query - subject
        ident_gene_num = len(query) - len(query_unique)
        SAG_dist = ident_gene_num / float(len(query))
        logging.debug(f"{str(time.time()-start)}秒, "\
                      f"gene_num_a: {str(len(seq_list_a))}, "\
                      f"gene_num_b: {str(len(seq_list_b))}, "\
                      f"SAG_dist: {str(SAG_dist)[:6]}")
        return SAG_dist