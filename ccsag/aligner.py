import logging
import os
import shutil
import subprocess

logger = logging.getLogger()

class Aligner:
    def __init__(self, fasta):
        self.fasta = fasta
        self.wdir = f"{os.getcwd()}/work"
        self.bam = f"{self.wdir}/merged.bam"
        logging.debug(f"class Aliner was created")
    
    def createindex(self):
        # method will be overrided in child class
        logging.debug("createindex method is undefined in class Aligner.")
    
    def mapping(self, fastq):
        # method will be overrided in child class
        logging.debug("mapping method is undefined in class Aligner.")
    
    def setWorkingDir(self, dir_name):
        self.wdir = dir_name
        self.bam = f"{self.wdir}/merged.bam"
        logging.debug(f"Working directory had been set. {self.wdir}")
    
    def clean(self):
        shutil.rmtree(self.wdir)
        logging.debug(f"Working directory had been removed. {self.wdir}")


class Minimap2(Aligner):

    def createindex(self):
        os.makedirs(self.wdir, exist_ok=True)
        
        # merge genome.fasta files
        cmd = ["cat"]
        fasta_list = [fasta.path for fasta in self.fasta]
        cmd.extend(list(fasta_list))
        ref_fasta = f"{self.wdir}/genomes.fasta"
        with open(ref_fasta, "w") as fo:
            subprocess.run(cmd, stdout=fo)
        
        # create index
        self.index = f"{self.wdir}/genomes.mmi"
        cmd = ["minimap2", "-x", "sr", "-d", self.index, ref_fasta]
        subprocess.run(cmd)
        logging.info(f"Mapping index has been created.")
    
    def mapping(self, fastq, cpu=1):
        if fastq.reverse:
            cmd1 = ["minimap2", "-ax", "sr", "-t", str(cpu), self.index, fastq.forward, fastq.reverse]
        else:
            cmd1 = ["minimap2", "-ax", "sr", "-t", str(cpu), self.index, fastq.forward]
        cmd2 = ["samtools", "view", "-b", "-F", "256"]
        self.bam = f"{self.wdir}/{fastq.name}_merged.bam"
        cmd3 = ["samtools", "sort", "-T", self.wdir, "-@", str(cpu), "-o", self.bam]
        logging.info(f"Fastq reads are being aligned: {fastq.name}")
        p1 = subprocess.Popen(cmd1, stdout=subprocess.PIPE)
        p2 = subprocess.Popen(cmd2, stdout=subprocess.PIPE, stdin=p1.stdout)
        subprocess.run(cmd3, stdin=p2.stdout)
