import csv
import itertools
import numpy as np
import os
import pysam
import re
import subprocess
import gzip
from collections import defaultdict
import shutil
import sys
from joblib import Parallel, delayed

class Checkm:
    class Record:
        def __init__(self, checkm_row):
            self.completeness = float(checkm_row['Completeness'])
            self.contamination = float(checkm_row['Contamination'])
            self.marker_lineage = checkm_row['Marker lineage']

    def __init__(self, dir_fasta, odir):
        self.dir_fasta = dir_fasta
        self.odir = odir
        self.result = os.path.join(odir, 'result.tsv')
        self.bin_stats = os.path.join(odir, 'storage/bin_stats_ext.tsv')
        self.data = {}
        self.marker_set = {}

    def _run_lineage_wf(self, x='fasta', cpu=1):
        cmd = ['checkm', 'lineage_wf', '--nt', '-x', x, '-t', str(cpu), '--tab_table',
               '-r', '-f', self.result, self.dir_fasta, self.odir]
        if not os.path.isfile(self.result):
            subprocess.run(' '.join(cmd), shell=True)

    def _run_taxonomy_wf(self, x='fasta', cpu=1):
        cmd = ['checkm', 'taxonomy_wf', '--nt', '-x', x, '-t', str(cpu), '--tab_table',
               '-f', self.result, 'domain', 'Bacteria', self.dir_fasta, self.odir]
        if not os.path.isfile(self.result):
            subprocess.run(' '.join(cmd), shell=True)

    def get_hmm(self, bin_id):
        return os.path.join(self.odir, 'bins', bin_id, 'hmmer.analyze.txt')

    def get_fna(self, bin_id):
        return os.path.join(self.odir, 'bins', bin_id, 'genes.fna')

    def run(self, x='fasta', cpu=1):
        self._run_lineage_wf(cpu=cpu, x=x)
        with open(self.result) as fh:
            r = csv.DictReader(fh, delimiter='\t')
            for row in r:
                self.data[row['Bin Id']] = Checkm.Record(row)
        with open(self.bin_stats) as fh:
            for line in fh:
                cols = line.rstrip().split('\t')
                self.marker_set[cols[0]] =  eval(cols[1])

class CompositeSag:
    @classmethod
    def identify_cluster(self, sag, pairs, composite_id, pref='strain'):
        if sag.composite_id is None:
            sag.composite_id = '{}-C{:05d}'.format(pref, composite_id)
        x = list(map(lambda v: v.sag2, filter(lambda v: v.sag1.sag_id == sag.sag_id, pairs)))
        y = list(map(lambda v: v.sag1, filter(lambda v: v.sag2.sag_id == sag.sag_id, pairs)))
        for same_sag in list(set(x) | set(y)):
            if same_sag.composite_id is None:
                CompositeSag.identify_cluster(same_sag, pairs, composite_id, pref)

class DirConf:
    def __init__(self, odir, intermediate, subdirs=[]):
        subdir = os.path.join(odir, intermediate)
        os.makedirs(subdir, exist_ok=True)
        self.__dict__ = {'odir': odir, 'intermediate': subdir}
        for dname in subdirs:
            path = os.path.join(self.intermediate, dname)
            self.__dict__[dname] = path
            os.makedirs(path, exist_ok=True)

class PairwiseSag:
    def __init__(self, sag1, sag2):
        self.sag1 = sag1
        self.sag2 = sag2
        self.is_same_strain = False
        self.common_markers = list(set(sag1.scg_markers) & set(sag2.scg_markers))
        self.jaccard = 0
        self.jaccard_n = 0
        self.jaccard_match = 0
        self.jaccard_total = 0
        self.ani = 0
        self.tnf_cor = 0

    def _get_blast(self, query, subject, blast_dir):
        oname = self._get_output(blast_dir)
        cmd = ['blastn', '-query', query, '-subject', subject, '-outfmt',
               '6 qseqid sseqid pident length qlen slen bitscore', '-out', oname]
        if not os.path.isfile(oname):
            subprocess.run(cmd)
        data = defaultdict(list)
        with open(oname) as fh:
            for line in fh:
                if line == '':
                    continue
                cols = line.split('\t')
                if cols[0] == cols[1]:
                    data[cols[0]].append({
                      'marker': cols[0],
                      'pident': float(cols[2]),
                      'length': int(cols[3]),
                      'qlen': int(cols[4]),
                      'slen': int(cols[5]),
                      'bitscore': float(cols[6]),
                    })
        maxdata = list(map(lambda v: max(v, key=lambda vv: vv['bitscore']), data.values()))
        return sorted(maxdata, key=lambda v: v['marker'])

    def _get_output(self, odir):
        name1 = os.path.basename(self.sag1.fasta)
        name2 = os.path.basename(self.sag2.fasta)
        return os.path.join(odir, '{}_{}'.format(name1, name2))

    def calc_ani(self, ani_dir, extfa='fasta'):
        oname = self._get_output(ani_dir)
        cmd = ['fastANI', '-r', self.sag1.fasta, '-q', self.sag2.fasta, '-o', oname]
        if not os.path.isfile(oname):
            subprocess.run(cmd)
        ani = []
        with open(oname) as fh:
            for line in fh:
                cols = line.rstrip().split('\t')
                cols[0] = os.path.basename(cols[0]).replace('.{}'.format(extfa), '')
                cols[1] = os.path.basename(cols[1]).replace('.{}'.format(extfa), '')
                ani.append(float(cols[2]))
        self.ani = ani[0] if len(ani) > 0 else 0

    def calc_jaccard(self, blast_dir, marker_size=1):
        if len(self.common_markers) >= marker_size:
            blast_res = self._get_blast(self.sag1.scg_fna, self.sag2.scg_fna, blast_dir)
            if len(blast_res) > 0:
                for v in blast_res:
                    v['qpart'] = self.sag1.scg_attr[v['marker']]['partial']
                    v['spart'] = self.sag2.scg_attr[v['marker']]['partial']
                match = list(map(lambda v: float(v['length']) * float(v['pident']) / 100.0, blast_res))
                total = list(map(lambda v: float(v['length']), blast_res))
                self.jaccard_n = len(blast_res)
                self.jaccard_match = sum(match)
                self.jaccard_total = sum(total)
                self.jaccard = self.jaccard_match / self.jaccard_total

    def calc_tnf(self):
        cor = np.corrcoef(self.sag1.tnf_freq, self.sag2.tnf_freq)
        self.tnf_cor = cor[0, 1]

class Sag:
    def __init__(self, sag_id):
        self.sag_id = sag_id
        self.fasta = None
        self.fastq1 = None
        self.fastq2 = None
        self.completeness = -1
        self.contamination = -1
        self.marker_lineage = None
        self.scg_attrs = {}
        self.scg_markers = []
        self.scg_fna = None
        self.composite_id = None
        self.tnf = None

    @classmethod
    def diff(self, sags1, sags2):
        diff = []
        sags2_id = dict(list(map(lambda sag: (sag.sag_id, True), sags2)))
        for sag in sags1:
            if sag.sag_id not in sags2_id:
                diff.append(sag)
        return diff

    @classmethod
    def unique(self, *sags_list):
        defined = {}
        unique = []
        for sags in sags_list:
            for sag in sags:
                if sag.sag_id not in defined:
                    unique.append(sag)
                    defined[sag.sag_id] = True
        return unique

class SingleCopyGene:
    def __init__(self):
        self.markers = []
        self.attr = {}

    def _each_marker(self, fname, accessions):
        data = dict(list(map(lambda acc: [acc, []], accessions)))
        with open(fname) as fh:
            for line in fh:
                if line.strip()[0] == '#':
                    continue
                cols = re.split(' +', line.rstrip())
                acc = re.sub('\.[0-9]+', '', cols[4])
                attr = dict(list(map(lambda v: v.split('='), cols[29].split(';'))))
                #if acc in data and attr['partial'] == '00':
                if acc in data:
                    attr['eval'] = float(cols[6])
                    attr['ieval'] = float(cols[12])
                    data[acc].append(attr)
        for acc, attrs in data.items():
            if len(attrs) > 0:
                attrs_sorted = sorted(attrs, key=lambda v: v['ieval'])
                yield(acc, attrs_sorted[0])

    def _get_marker_fasta(self, fa, marker_id):
        cmd = ['seqkit', 'grep', '-j', '1', '-nrp', 'ID={};'.format(marker_id), fa]
        res = subprocess.run(cmd, capture_output=True, text=True)
        return res.stdout.rstrip()

    def extract(self, fhmm, ffna, marker_set, ofna):
        for acc, info in self._each_marker(fhmm, marker_set['GCN1']):
            self.attr[acc] = info
            self.markers.append(acc)
        if os.path.exists(ofna):
            return
        with open(ofna, 'w') as fo:
            for acc, info in self.attr.items():
                marker = self._get_marker_fasta(ffna, info['ID'])
                markers = marker.split('\n')
                print('>{}'.format(acc), file=fo)
                print('\n'.join(markers[1:]), file=fo)

class Threshold:
    def __init__(self, dic):
        self.__dict__ = dic

class Tnf:
    def __init__(self, odir, fasta):
        self.fasta = fasta
        self.output = os.path.join(odir, '{}.jf'.format(os.path.basename(fasta)))
        self.freq = {}
        for pat in itertools.product(('A', 'C', 'G', 'T'), repeat=4):
            self.freq[''.join(pat)] = 0

    def _each_fasta(self, lines):
        id = None
        seqs = []
        for line in lines:
            if line[0] == '>':
                if id is not None:
                    yield id, ''.join(seqs)
                id = line.rstrip().replace('>', '')
                seqs = []
            else:
                seqs.extend(line.rstrip())
        if id is not None:
            yield id, ''.join(seqs)

    def calc(self):
        cmd = ['jellyfish', 'count', '-m', '4', '-s', '10000000', '-t', '1', '-o', self.output, self.fasta]
        subprocess.run(cmd)
        cmd = ['jellyfish', 'dump', self.output]
        proc = subprocess.run(cmd, stdout=subprocess.PIPE, encoding='utf-8')
        pat = ''
        for id, seq in self._each_fasta(proc.stdout.splitlines()):
            self.freq[seq] = int(id)

    def get_values(self):
        return list(map(lambda pat: self.freq[pat], self.get_pattern()))

    def get_pattern(self):
        return sorted(self.freq.keys())

def eval_pairwise_sag(pair, i, dir_marker, dir_ani, msize):
    pair.calc_jaccard(dir_marker, marker_size=msize)
    pair.calc_ani(dir_ani)
    pair.calc_tnf()
    return pair, i

def ccsag1_clustering(args, cont, marker_size, recursion_lim):
    #-- configure --#
    args.odir = f"{args.odir}/ccsag1_clustering"
    conf = DirConf(args.odir, args.wdir, ['fasta', 'checkm', 'sngl_cp_gn', 'marker_dist', 'fastani', 'tnf'])
    th = Threshold({'comp': args.comp, 'cont': cont, 'jaccard': args.marker, 'ani': args.ani, 'tnf_cor': args.tnf, 'msize': marker_size})

    #-- create fasta and fastq symbolic link and listing --#
    sags = []
    for fa in args.fasta:
        sag_id = os.path.splitext(os.path.basename(fa))[0]
        sag = Sag(sag_id)
        sag.fasta = fa
        sags.append(sag)
        shutil.copy(fa, os.path.join(conf.fasta, '{}.fasta'.format(sag_id)))

    #-- run checkm --#
    checkm = Checkm(conf.fasta, conf.checkm)
    checkm.run(cpu=args.t, x='fasta')

    #-- extract single copy marker genes from checkm result --#
    flt_sags = []
    flt_sag_dict = {}
    for sag in sags:
        if sag.sag_id not in checkm.data:
            continue
        sag.completeness = checkm.data[sag.sag_id].completeness
        sag.contamination = checkm.data[sag.sag_id].contamination
        sag.marker_lineage = checkm.data[sag.sag_id].marker_lineage
        if sag.completeness < th.comp or sag.contamination >= th.cont:
            continue
        fhmm = checkm.get_hmm(sag.sag_id)
        ffna = checkm.get_fna(sag.sag_id)
        sag.scg_fna = os.path.join(conf.sngl_cp_gn, '{}.fna'.format(sag.sag_id))
        sngl_cp_gn = SingleCopyGene()
        sngl_cp_gn.extract(fhmm, ffna, checkm.marker_set[sag.sag_id], sag.scg_fna)
        sag.scg_markers = sngl_cp_gn.markers
        sag.scg_attr = sngl_cp_gn.attr
        tnf = Tnf(conf.tnf, sag.fasta)
        tnf.calc()
        sag.tnf_pat = tnf.get_pattern()
        sag.tnf_freq = tnf.get_values()
        flt_sags.append(sag)
        flt_sag_dict[sag.sag_id] = sag

    #-- create PairwiseSag object for comparison --#
    n = len(flt_sags)
    pairs = []
    for i in range(0, n - 1):
        for j in range(i + 1, n):
            pairs.append(PairwiseSag(flt_sags[i], flt_sags[j]))
    res = Parallel(n_jobs=args.t)([delayed(eval_pairwise_sag)(pair, i, conf.marker_dist, conf.fastani, th.msize) for i, pair in enumerate(pairs)])
    res.sort(key=lambda x: x[1])
    pairs = list(map(lambda x: x[0], res))
    for pair in pairs:
        pair.sag1 = flt_sag_dict[pair.sag1.sag_id]
        pair.sag2 = flt_sag_dict[pair.sag2.sag_id]
        pair.is_same_strain = pair.ani >= th.ani and pair.jaccard >= th.jaccard and pair.tnf_cor >= th.tnf_cor
    same_pairs = list(filter(lambda v: v.is_same_strain, pairs))

    #-- identify composite SAG --#
    composite_id = 1
    paired_sags = list(set(map(lambda v: v.sag1, same_pairs)) | set(map(lambda v: v.sag2, same_pairs)))
    sys.setrecursionlimit(recursion_lim)
    for sag in sorted(paired_sags, key=lambda s: s.sag_id):
        if sag.composite_id is None:
            CompositeSag.identify_cluster(sag, same_pairs, composite_id, args.pref)
            composite_id += 1

    #-- output pairwise sag --#
    with open(os.path.join(args.odir, args.wdir, 'pairwise_sag.txt'), 'w') as fo:
        print('\t'.join(['sag1', 'sag2', 'is_same_strain', 'num_of_markers', 'sig_markers', 'jaccard', 'ani', 'tnf']), file=fo)
        for pair in pairs:
            print('\t'.join([
              pair.sag1.sag_id,
              pair.sag2.sag_id,
              str(pair.is_same_strain),
              str(len(pair.common_markers)),
              str(pair.jaccard_n),
              str(pair.jaccard),
              str(pair.ani),
              str(pair.tnf_cor),
            ]), file=fo)

    #-- output sag2csag --#
    with open(os.path.join(args.odir, 'clustering_result.txt'), 'w') as fo:
        print('\t'.join(['sag_id', 'csag_id', 'completeness', 'contamination', 'marker_lineage', 'is_composite']), file=fo)
        for sag in sorted(paired_sags, key=lambda s: (s.composite_id, -s.completeness)):
            print('\t'.join([
              sag.sag_id,
              sag.composite_id,
              str(sag.completeness),
              str(sag.contamination),
              sag.marker_lineage,
              'True',
            ]), file=fo)
        #for sag in sorted(Sag.diff(flt_sags, paired_sags), key=lambda s: s.completeness, reverse=True):
        for sag in sorted(list(set(flt_sags) - set(paired_sags)), key=lambda s: s.completeness, reverse=True):
            print('\t'.join([
              sag.sag_id,
              sag.sag_id,
              str(sag.completeness),
              str(sag.contamination),
              sag.marker_lineage,
              'False',
            ]), file=fo)

    #-- remove intermediate --#
    if not args.keep:
        shutil.rmtree(conf.checkm)
        shutil.rmtree(conf.sngl_cp_gn)
        shutil.rmtree(conf.marker_dist)
        shutil.rmtree(conf.fastani)
        shutil.rmtree(conf.tnf)
        shutil.rmtree(conf.fasta)
