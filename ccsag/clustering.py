import json
from kneed import KneeLocator
import logging
import numpy as np
import os
import pandas as pd
import shutil
from sklearn import cluster
from sklearn.neighbors import NearestNeighbors

logger = logging.getLogger()

class Clustering:
    def __init__(self, dist):
        self.dist = dist
        self.cluster = {}
        self.method = "undefined"
        self.wdir = f"{os.getcwd()}/work"
        logging.debug("class Clustering had been initialized.")
    
    # clustering SAGs
    def clustering(self):
        # method will be overrided in child class
        logging.debug("clustering method is undefined in class Clustering.")
        
    def _saveCluster(self, samples, cluster_labels):

        # assign the clustering result
        result_dict = {}
        for data in zip(samples, cluster_labels):
            sample_name, label = data[0], str(data[1])
            if label not in result_dict: result_dict[label] = []
            result_dict[label].append(sample_name)
        self.cluster = result_dict
        
        # save clustering result as a json file
        ofile = f"{self.wdir}/{self.method}_result.json"
        with open(ofile, "w") as o:
            json.dump(result_dict, o, indent=2)
        logging.info(f"{self.method} result was saved as '{ofile}'")
        
        # save clustering result as a txt file
        ofile = f"{self.wdir}/{self.method}_result.txt"
        otext = ["Sample\tClusterid"]
        for data in zip(samples, cluster_labels):
            sample_name, label = data[0], str(data[1])
            otext.append("\t".join([sample_name, label]))
        with open(ofile, "w") as o:
            o.write("\n".join(otext))
        logging.info(f"{self.method} result was saved as '{ofile}'")
        
        
    # return cluster labels and clustered SAGs
    def getCluster(self):
        return self.cluster
    
    def setWorkingDir(self, dir_name):
        self.wdir = dir_name
        logging.debug(f"Working directory had been set. {self.wdir}")
    
    def clean(self):
        shutil.rmtree(self.wdir)
        logging.debug(f"Working directory had been removed. {self.wdir}")


class DBSCAN(Clustering):

    def __init__(self, dist):
        super().__init__(dist)
        self.method = "DBSCAN"
        self.wdir = f"{self.wdir}/{self.method}"
        logging.debug(f"class {self.method} had been initialized.")
    
    def clustering(self):
        logging.info(f"SAG clustering by {self.method} started.")
        
        os.makedirs(self.wdir, exist_ok=True)
        
        # convert input format from dict to np.array
        df = pd.DataFrame(self.dist).fillna(0)
        df.index = [SAG.name for SAG in df.index.tolist()]
        df.columns = [SAG.name for SAG in df.columns.tolist()]
        data_set = df.to_numpy()
        
        # nearest neighbor search
        nearest_neighbors = NearestNeighbors(n_neighbors=3)
        neighbors = nearest_neighbors.fit(data_set)
        distances, indices = neighbors.kneighbors(data_set)
        distances = np.sort(distances[:,2], axis=0)
        
        # tune dbscan eps parameter
        i = np.arange(len(distances))
        knee = KneeLocator(i, distances, S=1, curve='convex', direction='increasing', interp_method='polynomial')
        # DBSCAN
        clustering_result = cluster.DBSCAN(eps=distances[knee.knee], min_samples=2, metric='euclidean').fit_predict(data_set)
        
        # output clustering result
        samples = df.index
        self._saveCluster(samples, clustering_result)
        
        logging.info(f"{self.method} finished.")


class SetCover(Clustering):

    def __init__(self, dist):
        super().__init__(dist)
        self.method = "SetCover"
        self.wdir = f"{self.wdir}/{self.method}"
        logging.debug(f"class {self.method} had been initialized.")
    
    def clustering(self):
        logging.info(f"SAG clustering by {self.method} started.")
        
        os.makedirs(self.wdir, exist_ok=True)
        
        # convert input format from dict to df
        df = pd.DataFrame(self.dist).fillna(0)
        df.index = [SAG.name for SAG in df.index.tolist()]
        df.columns = [SAG.name for SAG in df.columns.tolist()]
        distdf = df
        
        # set cover clustering
        threshold = 0.05
        labelname = 0
        labeldict = {}
        distdf["arm_num"] = (distdf >= threshold).sum(axis = 1)
        while distdf["arm_num"].max() > 1:
            target = distdf[distdf["arm_num"] == distdf["arm_num"].max()].index[0]
            saglist = distdf.index
            for sag in saglist:
                if distdf.loc[sag, target] < threshold: continue
                if sag == target: continue
                labeldict[sag] = str(labelname)
                distdf = distdf.drop(sag, axis=0)
                distdf = distdf.drop(sag, axis=1)
            else:
                labeldict[target] = str(labelname)
                distdf = distdf.drop(target, axis=0)
                distdf = distdf.drop(target, axis=1)
            labelname += 1
            distdf = distdf.drop("arm_num", axis = 1)
            distdf["arm_num"] = (distdf >= threshold).sum(axis = 1)
        else:
            for sag in distdf.index:
                labeldict[sag] = "-1"
        
        # output clustering result
        samples = sorted(labeldict.keys())
        clustering_result = [labeldict[sample] for sample in samples]
        self._saveCluster(samples, clustering_result)
        
        logging.info(f"{self.method} finished.")
        

class Star(Clustering):
    
    def __init__(self, dist):
        super().__init__(dist)
        self.method = "Star"
        self.wdir = f"{self.wdir}/{self.method}"
        logging.debug(f"class {self.method} had been initialized.")
    
    def clustering(self):
        logging.info(f"SAG clustering by {self.method} started.")
        
        os.makedirs(self.wdir, exist_ok=True)
        
        # convert input format from dict to df
        df = pd.DataFrame(self.dist).fillna(0)
        df.index = [SAG.name for SAG in df.index.tolist()]
        df.columns = [SAG.name for SAG in df.columns.tolist()]
        distdf = df
        
        # Star Clustering
        model = StarCluster()
        model.fit(distdf.to_numpy())
        labellist = model.predict(distdf.to_numpy())
        result = []
        result = self.__recursive_clustering(distdf, labellist, result)
        
        # output clustering result
        samples = [data[0] for data in result]
        clustering_result = [data[1] for data in result]
        self._saveCluster(samples, clustering_result)
        
        logging.info(f"{self.method} finished.")
    
    def __recursive_clustering(self, df, labellist, result):
        labelset = sorted(list(set(labellist)))
        for label in labelset:
            sampleid = []
            tdf = df
            for i in reversed(range(len(labellist))):
                if labellist[i] != label:
                    tdf = tdf.drop(tdf.columns[[i]], axis=1)
                    tdf = tdf.drop(tdf.index[[i]])
                else:
                    sampleid.append(tdf.index[i])
            if len(sampleid) <= 2:
                result += [[sampleid[i], label] for i in range(len(sampleid))]
                continue
            model = StarCluster()
            model.fit(tdf.to_numpy())
            newlabels = [f"{label}_{i}" for i in reversed(model.predict(tdf.to_numpy()))]
            if len(set(newlabels)) == 1:
                result += [[sampleid[i], newlabels[i]] for i in range(len(sampleid))]
            else:
                self.__recursive_clustering(tdf, newlabels, result)
        return result
        

# Copyright 2020 Joseph Lin Chu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
class Euclidean(object):
 
    @staticmethod
    def calc(vectors, center_cols=False, center_rows=False):
        n = vectors.shape[0]
        d = vectors.shape[1]
        target = np.zeros((n, n, d), dtype='float32')
 
        if center_cols:
            vectors = vectors - np.expand_dims(np.average(vectors, axis=0), axis=0)
        if center_rows:
            vectors = vectors - np.expand_dims(np.average(vectors, axis=-1), axis=-1)
 
        for i in range(n):
            for j in range(n):
                target[i, j] = vectors[i] - vectors[j]
 
        target = np.linalg.norm(target, axis=-1)
        return target

class StarCluster(object):
    """Star Clustering Algorithm"""
 
    def fit(self, X, upper=False, limit_exp=1, dis_type='euclidean'):
        # Set Constant of Proportionality
        golden_ratio = ((1.0 + (5.0 ** 0.5)) / 2.0)
        # Number of Nodes
        n = X.shape[0]
        # Number of Features
        d = X.shape[1]
 
        dis_class = Euclidean
 
        # Construct Node-To-Node Matrix of Distances
        distances_matrix = dis_class.calc(X)
 
        # Determine Average Distance And Extend By Constant of Proportionality To Set Limit
        limit = np.sum(distances_matrix) / (n * n - n) * golden_ratio ** limit_exp
 
        # Construct List of Distances Less Than Limit
        distances_list = []
        for i in range(n):
            for j in range(n):
                if i < j:
                    distances_list.append((i, j, distances_matrix[i, j]))
 
        # Sort List of Distances From Shortest To Longest
        distances_list.sort(key=lambda x: x[2])
 
        # Build Matrix of Connections Starting With Closest Pairs Until Average Mass Exceeds Limit
        empty_clusters = []
        mindex = 0
        self.labels_ = np.zeros(n, dtype='int32') - 1
        if upper:
            self.ulabels = np.zeros(n, dtype='int32')
        mass = np.zeros(n, dtype='float32')
        while np.mean(mass) <= limit:
            i, j, distance = distances_list[mindex]
            mass[i] += distance
            mass[j] += distance
            if self.labels_[i] == -1 and self.labels_[j] == -1:
                if not empty_clusters:
                    empty_clusters = [np.max(self.labels_) + 1]
                empty_clusters.sort()
                cluster = empty_clusters.pop(0)
                self.labels_[i] = cluster
                self.labels_[j] = cluster
            elif self.labels_[i] == -1:
                self.labels_[i] = self.labels_[j]
            elif self.labels_[j] == -1:
                self.labels_[j] = self.labels_[i]
            elif self.labels_[i] < self.labels_[j]:
                empty_clusters.append(self.labels_[j])
                self.labels_[np.argwhere(self.labels_ == self.labels_[j])] = self.labels_[i]
            elif self.labels_[j] < self.labels_[i]:
                empty_clusters.append(self.labels_[i])
                self.labels_[np.argwhere(self.labels_ == self.labels_[i])] = self.labels_[j]
            mindex += 1
 
        distances_matrix[distances_matrix == 0.0] = np.inf
        # Reduce Mass of Each Node By Effectively Twice The Distance To Nearest Neighbour
        for i in range(n):
            mindex = np.argmin(distances_matrix[i])
            distance = distances_matrix[i, mindex]
            mass[i] -= distance
            mass[mindex] -= distance
 
        # Set Threshold Based On Average Modified By Deviation Reduced By Constant of Proportionality
        threshold = np.mean(mass) - np.std(mass) / golden_ratio
 
        # Disconnect Lower Mass Nodes
        for i in range(n):
            if mass[i] <= threshold:
                self.labels_[i] = -1
        if upper:
            uthreshold = np.mean(mass) + np.std(mass) / golden_ratio
            for i in range(n):
                if mass[i] >= uthreshold:
                    self.ulabels[i] = -1
 
        # Ignore Masses of Nodes In Clusters Now
        mass[self.labels_ != -1] = -np.inf
        acount = 0
        # Go Through Disconnected Nodes From Highest To Lowest Mass And Reconnect To Nearest Neighbour That Hasn't Already Connected To It Earlier
        while -1 in self.labels_:
            i = np.argmax(mass)
            mindex = i
            if upper:
                while self.labels_[mindex] == -1:
                    dsorted = np.argsort(distances_matrix[i])
                    not_connected = True
                    sidx = 0
                    while not_connected:
                        cidx = dsorted[sidx]
                        if self.ulabels[cidx] == -1:
                            acount += 1
                            sidx += 1
                        else:
                            mindex = cidx
                            not_connected = False
                    distances_matrix[i, mindex] = np.inf
            else:
                while self.labels_[mindex] == -1:
                    mindex = np.argmin(distances_matrix[i])
                    distance = distances_matrix[i, mindex]
                    distances_matrix[i, mindex] = np.inf
            self.labels_[i] = self.labels_[mindex]
            mass[i] = -np.inf
 
        if upper:
            print('Connections to nodes above upper mass threshold skipped: {}'.format(acount))
        return self
 
    def predict(self, X):
        self.fit(X)
        return self.labels_

def clustering(df, labellist, result):
    labelset = sorted(list(set(labellist)))
    for label in labelset:
        sampleid = []
        tdf = df
        for i in reversed(range(len(labellist))):
            if labellist[i] != label:
                tdf = tdf.drop(tdf.columns[[i]], axis=1)
                tdf = tdf.drop(tdf.index[[i]])
            else:
                sampleid.append(tdf.index[i])
        if len(sampleid) <= 2:
            result += [[sampleid[i], label] for i in range(len(sampleid))]
            continue
        model = StarCluster()
        model.fit(tdf.to_numpy())
        newlabels = [f"{label}_{i}" for i in reversed(model.predict(tdf.to_numpy()))]
        if len(set(newlabels)) == 1:
            result += [[sampleid[i], newlabels[i]] for i in range(len(sampleid))]
        else:
            clustering(tdf, newlabels, result)
    return result