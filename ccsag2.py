#!/usr/bin/env python
import argparse
import logging
import os
import pathlib
import shutil
import sys
import time

from ccsag.seq_data_obj import Fasta, Fastq, Bam
from ccsag.bruteforce import Jaccard
from ccsag.clustering import DBSCAN, SetCover, Star
from ccsag.aligner import Minimap2
from ccsag.libccsag1 import Checkm, CompositeSag, DirConf, PairwiseSag, Sag, SingleCopyGene, Threshold, Tnf
from ccsag.libccsag1 import ccsag1_clustering

### default parameters
odir = "./ccsag_result"
wdir = f"{odir}/work"
cpu = 1
min_read_len = 21
PREF = 'Strain'
COMP = 20.0
CONT = 10.0
JACCARD = 0.99
TNF_COR = 0.9
MARKER_SIZE = 1
ANI = 95.0
RECURSION_LIM = 2000


logger = logging.getLogger()

def set_log_file(args):
    log_txt = pathlib.Path(f'{args.odir}/log.txt')

    fh = logging.FileHandler(log_txt, encoding='utf-8', mode='w')
    fh.setLevel(logging.INFO)
    fh.setFormatter(logging.Formatter('%(asctime)s %(levelname)s %(funcName)s: %(message)s'))

    root_logger = logging.getLogger()
    root_logger.setLevel(logging.DEBUG)
    root_logger.addHandler(fh)

def exit_with_error(msg):
    logging.info(msg)
    sys.exit(msg)

def proc_clustering_ccsag1(args):
    ccsag1_clustering(args, CONT, MARKER_SIZE, RECURSION_LIM)

def proc_clustering(args):
    
    args.odir = f"{args.odir}/SAG_clustering"
    os.makedirs(args.odir, exist_ok=True)
    set_log_file(args)
    
    # check input gene.fna files
    if len(args.fna) == 1:
        exit_with_error('SAG clustering requires multiple gene.fna files.')
    for fastafile in args.fna:
        if not os.path.isfile(fastafile):
            exit_with_error(f'FileNotFoundError: No such file or directory: {fastafile}')
        
    
    logging.info('SAG clustering started.')
    start = time.time()
    
    # make Fasta object
    fastas = [Fasta(fastafile) for fastafile in args.fna]
    
    # calculate SAG-SAG distances
    dist_wdir = f"{args.wdir}/Distance"
    dist_calc_obj = Jaccard(*fastas)
    dist_calc_obj.setWorkingDir(dist_wdir)
    dist_calc_obj.calcDist()
    
    # cluster SAGs
    clust_wdir = f"{args.wdir}/Clustering"
    if args.method == "all" or args.method == "SetCover":
        clustering_obj = SetCover(dist_calc_obj.getDist())
        clustering_obj.setWorkingDir(clust_wdir)
        clustering_obj.clustering()
    if args.method == "all" or args.method == "DBSCAN":
        clustering_obj = DBSCAN(dist_calc_obj.getDist())
        clustering_obj.setWorkingDir(clust_wdir)
        clustering_obj.clustering()
    if args.method == "all" or args.method == "Star":
        clustering_obj = Star(dist_calc_obj.getDist())
        clustering_obj.setWorkingDir(clust_wdir)
        clustering_obj.clustering()
    
    # save results
    intermediate_file_list =  os.listdir(clust_wdir)
    for intermediate_file in intermediate_file_list:
        if intermediate_file[-4:] != "json": continue
        shutil.copy(os.path.join(clust_wdir, intermediate_file), args.odir)
    
    # remove intermediate files
    if args.keep != True:
        shutil.rmtree(dist_wdir)
        shutil.rmtree(clust_wdir)
        logging.info("Intermediate files have been removed.")
    
    logging.info(f"SAG clustering finished. ({str(time.time()-start)} s.)")


def proc_cleaning(args):
    
    args.odir = f"{args.odir}/Cleaning"
    os.makedirs(args.odir, exist_ok=True)
    set_log_file(args)
    
    
    logging.info('fastq cleaning started.')
    start = time.time()
    
    # align fastq reads to genomic sequences
    fastas = [Fasta(fastafile) for fastafile in args.fasta]
    aligner = Minimap2(fastas)
    align_wdir = f"{args.wdir}/Alignment"
    aligner.setWorkingDir(align_wdir)
    aligner.createindex()
    fastq = Fastq(args.fastq, args.reverse)
    aligner.mapping(fastq, args.threads)
    
    # clean fastq reads
    ofile = f"{args.odir}/{fastq.name}_cleaned.fastq.gz"
    bam = Bam(aligner.bam)
    bam.rmChimera(ofile, args.minlen)
    
    # remove intermediate files
    if args.keep != True:
        shutil.rmtree(align_wdir)
        logging.info("Intermediate files have been removed.")
    
    logging.info(f"fastq cleaning finished. ({str(time.time()-start)} s.)")


### options
# common
def opt_odir(parser):
    parser.add_argument('-o', '--output-dir', default=odir, action='store',
                        dest='odir', help=f'directory to write output files. default: {odir}')

def opt_wdir(parser):
    parser.add_argument('-w', '--working-dir', default=wdir, action='store',
                        dest='wdir', help=f'directory to write intermediate files. default: {wdir}')

def opt_keep_intermediate(parser):
    parser.add_argument('--keep-intermediate', action='store_true', dest='keep',
                        help='do not remove intermediate files')

# clustering
def opt_clust_ifna(parser):
    parser.add_argument('fna', action='store', nargs='+',
                        help='fna file including CDS sequences of each genome')
    
def opt_clust_method(parser):
    parser.add_argument('method', action='store',
                        choices=['SetCover', 'DBSCAN', 'Star', 'all'],
                        help='Choose clustering method')


def opt_ccsag1_ifasta(parser):
    parser.add_argument('fasta', action='store', nargs='+',
                        help='fasta file of each genome')

def opt_ccsag1_comp(parser):
    parser.add_argument('-c', '--completeness', dest='comp', action='store', metavar='FLOAT', type=float,
                    default=COMP, help='SAGs less than this value are discarded, default={}'.format(COMP))

def opt_ccsag1_criteria(parser):
    parser.add_argument('--criteria-ani', dest='ani', action='store', metavar='FLOAT', type=float,
                    default=ANI, help='ANI criteria to identify same strain, default={}'.format(ANI))
    parser.add_argument('--criteria-marker', dest='marker', action='store', metavar='FLOAT', type=float,
                    default=JACCARD, help='ANI criteria to identify same strain, default={}'.format(JACCARD))
    parser.add_argument('--criteria-tnf', dest='tnf', action='store', metavar='FLOAT', type=float,
                    default=TNF_COR, help='TNF correlation criteria to identify same strain, default={}'.format(TNF_COR))

def opt_ccsag1_pref(parser):
    parser.add_argument('-p', '--prefix', dest='pref', action='store', metavar='STR',
                    default=PREF, help='composite SAG prefix, default={}'.format(PREF))

def opt_ccsag1_threads(parser):
    parser.add_argument('-t', '--threads', dest='t', action='store', metavar='INT', type=int,
                    default=cpu, help='number of threads, default={}'.format(cpu))

# cleaning
def opt_clean_ifa(parser):
    parser.add_argument('fasta', action='store', nargs='+',
                        help='fasta file of each genome')

def opt_clean_ifq(parser):
    parser.add_argument('fastq', action='store', help='input fastq (fastq.gz only)')

def opt_clean_paired(parser):
    parser.add_argument('-p', default=None, action="store", dest='reverse',
                        help="peired-end fastq of input fastq (fastq.gz only)")

def opt_clean_minlen(parser):
    parser.add_argument('-m', '--minimum-length', dest='minlen', action='store', type=int,
                        default=min_read_len, help=f'reads less than this value are discarded, default={min_read_len}')

def opt_clean_threads(parser):
    parser.add_argument('-t', '--threads', dest='threads', action='store', type=int,
                        default=cpu, help=f'number of threads, default={cpu}')

def set_options(parser, funcs, cmd):
    for func in funcs:
        func(parser)
    parser.set_defaults(handler=cmd)

def set_parser():
    parser = argparse.ArgumentParser('ccsag')
    subparsers = parser.add_subparsers()
    
    # set clustering command
    parser_clustering = subparsers.add_parser('clustering-fast', help='see `clustering-fast -h`')
    set_options(parser_clustering,
                (opt_odir, opt_wdir, opt_keep_intermediate, opt_clust_method, opt_clust_ifna),
                proc_clustering)
    
    # set clustering command
    parser_clustering_ccsag1 = subparsers.add_parser('clustering-careful', help='see `clustering-careful -h`')
    set_options(parser_clustering_ccsag1,
                (opt_odir, opt_wdir, opt_keep_intermediate, opt_ccsag1_comp, opt_ccsag1_criteria,
                 opt_ccsag1_pref, opt_ccsag1_threads, opt_ccsag1_ifasta),
                proc_clustering_ccsag1)
    
    # set cleaning command
    parser_cleaning = subparsers.add_parser('cleaning', help='see `cleaning -h`')
    set_options(parser_cleaning,
                (opt_odir, opt_wdir, opt_keep_intermediate, opt_clean_minlen,
                 opt_clean_threads, opt_clean_paired, opt_clean_ifq, opt_clean_ifa),
                proc_cleaning)
    
    return parser
    

def main():
    
    parser = set_parser()
    args = parser.parse_args()
    if hasattr(args, 'handler'):
        args.handler(args)
    else:
        parser.print_help()
    
    sys.exit()

if __name__ == '__main__':
    main()
    
