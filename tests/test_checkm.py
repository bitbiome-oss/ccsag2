from unittest import TestCase
from ccsag.libccsag1 import Checkm
import gzip
import os
import shutil

class TestCheckm(TestCase):
    def setUp(self):
        self.odir = 'tests/output/checkm'
        self.dfasta = os.path.join(self.odir, 'fasta')
        if os.path.isdir(self.odir):
            shutil.rmtree(self.odir)

        os.makedirs(self.dfasta, exist_ok=True)
        fa_lst = [
            'tests/data/test1_mock19.bifido.fasta.gz',
            'tests/data/test2_mock3.bifido.fasta.gz',
            'tests/data/test3_mock4.bifido.fasta.gz',
            'tests/data/test4_mock10.acnes.fasta.gz',
            'tests/data/test5_mock25.acnes.fasta.gz',
            'tests/data/test6_mock9.acnes.fasta.gz',
        ]
        for src in fa_lst:
            filename = os.path.splitext(os.path.basename(src))[0]
            dst = os.path.join(self.dfasta, filename)
            with gzip.open(src, mode="rb") as fh:
                with open(dst, mode="wb") as fo:
                    shutil.copyfileobj(fh, fo)

    def test_run(self):
        checkm = Checkm(self.dfasta, self.odir)
        checkm.run(cpu=4)

        self.assertEqual(len(checkm.data.keys()), 6)
        self.assertTrue('test1_mock19.bifido' in checkm.data)
        self.assertTrue('test6_mock9.acnes' in checkm.data)
        self.assertTrue(os.path.exists(self.odir))
        self.assertTrue(os.path.exists(checkm.get_hmm('test1_mock19.bifido')))
        self.assertTrue(os.path.exists(checkm.get_fna('test1_mock19.bifido')))
        self.assertTrue(os.path.exists(checkm.get_hmm('test6_mock9.acnes')))
        self.assertTrue(os.path.exists(checkm.get_fna('test6_mock9.acnes')))

