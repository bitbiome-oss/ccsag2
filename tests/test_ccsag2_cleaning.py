from unittest import TestCase
import subprocess
import os
import json
import shutil
import gzip

class TestCcsag2Cleaning(TestCase):
    def test_cleaning(self):
        odir = 'tests/output/ccsag2/cleaning'
        if os.path.isdir(odir):
            shutil.rmtree(odir)
        cmd = [
          'python ./ccsag2.py cleaning',
          '-o', odir,
          '-w', os.path.join(odir, 'work'),
          '-p', 'tests/data/test1_mock19.bifido_2.fastq.gz',
          'tests/data/test1_mock19.bifido_1.fastq.gz',
          'tests/data/test2_mock3.bifido.fasta.gz',
          'tests/data/test3_mock4.bifido.fasta.gz',
        ]
        subprocess.run(' '.join(cmd), shell=True)

        self.assertTrue(os.path.exists(os.path.join(odir, 'Cleaning/test1_mock19.bifido_1_cleaned.fastq.gz')))
        self.assertFalse(os.path.isdir(os.path.join(odir, 'work/Alignment')))

        with gzip.open(os.path.join(odir, 'Cleaning/test1_mock19.bifido_1_cleaned.fastq.gz')) as fh:
            for line in fh:
                id1  = line.strip()
                seq  = fh.readline().strip()
                id2  = fh.readline().strip()
                qual = fh.readline().strip()
                if id1 == '@M03360:322:000000000-CNGCG:1:1101:18142:8632:2:1':
                    assertEqual(seq, 'GATGTGCAGACACGGAACGCGGGATGTGCAGACACGGAACGCGGGATGTGCAGACGGCAATTA')
                    break

    def test_cleaning_keep(self):
        odir = 'tests/output/ccsag2/cleaning-keep'
        if os.path.isdir(odir):
            shutil.rmtree(odir)
        cmd = [
          'python ./ccsag2.py cleaning',
          '-o', odir,
          '-w', os.path.join(odir, 'work'),
          '--keep-intermediate',
          '-p', 'tests/data/test1_mock19.bifido_2.fastq.gz',
          'tests/data/test1_mock19.bifido_1.fastq.gz',
          'tests/data/test2_mock3.bifido.fasta.gz',
          'tests/data/test3_mock4.bifido.fasta.gz',
        ]
        subprocess.run(' '.join(cmd), shell=True)

        self.assertTrue(os.path.exists(os.path.join(odir, 'Cleaning/test1_mock19.bifido_1_cleaned.fastq.gz')))
        self.assertTrue(os.path.isdir(os.path.join(odir, 'work/Alignment')))

        with gzip.open(os.path.join(odir, 'Cleaning/test1_mock19.bifido_1_cleaned.fastq.gz')) as fh:
            for line in fh:
                id1  = line.strip()
                seq  = fh.readline().strip()
                id2  = fh.readline().strip()
                qual = fh.readline().strip()
                if id1 == '@M03360:322:000000000-CNGCG:1:1101:18142:8632:2:1':
                    assertEqual(seq, 'GATGTGCAGACACGGAACGCGGGATGTGCAGACACGGAACGCGGGATGTGCAGACGGCAATTA')
                    break

