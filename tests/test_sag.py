from unittest import TestCase
from ccsag.libccsag1 import Sag
import gzip
import os

class TestSag(TestCase):
    def test_sag_id(self):
        sag = Sag('BBD001-00001')
        self.assertEqual(sag.sag_id, 'BBD001-00001')

    def test_diff(self):
        sags1 = [
            Sag('BBD001-00001'),
            Sag('BBD001-00002'),
            Sag('BBD001-00003'),
            Sag('BBD001-00004'),
        ]
        sags2 = [
            Sag('BBD001-00005'),
            Sag('BBD001-00002'),
            Sag('BBD001-00003'),
        ]
        sags_diff = Sag.diff(sags1, sags2)

        self.assertEqual(len(sags_diff), 2)
        self.assertEqual(sags_diff[0].sag_id, 'BBD001-00001')
        self.assertEqual(sags_diff[1].sag_id, 'BBD001-00004')

    def test_unique(self):
        sags1 = [
            Sag('BBD001-00001'),
            Sag('BBD001-00002'),
            Sag('BBD001-00003'),
            Sag('BBD001-00004'),
        ]
        sags2 = [
            Sag('BBD001-00005'),
            Sag('BBD001-00002'),
            Sag('BBD001-00003'),
        ]
        sags_uniq = Sag.unique(sags1, sags2)

        self.assertEqual(len(sags_uniq), 5)
        self.assertEqual(sags_uniq[0].sag_id, 'BBD001-00001')
        self.assertEqual(sags_uniq[1].sag_id, 'BBD001-00002')
        self.assertEqual(sags_uniq[2].sag_id, 'BBD001-00003')
        self.assertEqual(sags_uniq[3].sag_id, 'BBD001-00004')
        self.assertEqual(sags_uniq[4].sag_id, 'BBD001-00005')
