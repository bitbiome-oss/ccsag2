from unittest import TestCase
from ccsag.libccsag1 import SingleCopyGene
import gzip
import os
import shutil

class TestSingleCopyGene(TestCase):
    def setUp(self):
        dir_checkm = 'tests/data/checkm'
        self.odir = 'tests/output/single_copy_gene'
        if os.path.isdir(self.odir):
            shutil.rmtree(self.odir)
        os.makedirs(self.odir)
        self.fhmm = os.path.join(dir_checkm, 'bins/test1_mock19.bifido/hmmer.analyze.txt')
        self.ffna = os.path.join(dir_checkm, 'bins/test1_mock19.bifido/genes.fna')
        self.marker_set = {}
        bin_stats = os.path.join(dir_checkm, 'storage/bin_stats_ext.tsv')
        with open(bin_stats) as fh:
            for line in fh:
                cols = line.rstrip().split('\t')
                self.marker_set[cols[0]] = eval(cols[1])

    def test_extract(self):
        ofna = 'tests/output/single_copy_gene/test1_mock19.bifido.fna'
        spg = SingleCopyGene()
        spg.extract(self.fhmm, self.ffna, self.marker_set['test1_mock19.bifido'], ofna)

        self.assertTrue(os.path.exists(ofna))
        self.assertEqual(len(spg.markers), 38)
        self.assertEqual(len(spg.attr.keys()), 38)
        self.assertTrue(spg.markers[0] in spg.attr)
        self.assertTrue(spg.markers[37] in spg.attr)

