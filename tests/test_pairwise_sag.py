from unittest import TestCase
from ccsag.libccsag1 import PairwiseSag, Sag, SingleCopyGene, Tnf
import gzip
import os
import shutil

def build_sag(rec, marker_set, odir):
    sag = Sag(rec['id'])
    with gzip.open(rec['fai'], mode="rb") as fh:
        with open(rec['fao'], mode="wb") as fo:
            shutil.copyfileobj(fh, fo)
    sag.fasta = rec['fao']

    spg = SingleCopyGene()
    ofna = os.path.join(odir, '{}.marker.fna'.format(rec['id']))
    spg.extract(rec['hmm'], rec['fna'], marker_set[rec['id']], ofna)
    sag.scg_markers = spg.markers
    sag.scg_attr = spg.attr
    sag.scg_fna = ofna

    tnf = Tnf(odir, rec['fao'])
    tnf.calc()
    sag.tnf_pat = tnf.get_pattern()
    sag.tnf_freq = tnf.get_values()

    return sag

class TestPairwiseSag(TestCase):
    def setUp(self):
        self.odir = 'tests/output/pairwise_sag'
        os.makedirs(self.odir, exist_ok=True)

        dir_checkm = 'tests/data/checkm'
        bin_stats = os.path.join(dir_checkm, 'storage/bin_stats_ext.tsv')
        marker_set = {}
        with open(bin_stats) as fh:
            for line in fh:
                cols = line.rstrip().split('\t')
                marker_set[cols[0]] = eval(cols[1])

        test_data = [
            {
                'id': 'test1_mock19.bifido',
                'fai': 'tests/data/test1_mock19.bifido.fasta.gz',
                'fao': os.path.join(self.odir, 'test1_mock19.bifido.fasta'),
                'hmm': os.path.join(dir_checkm, 'bins/test1_mock19.bifido/hmmer.analyze.txt'),
                'fna': os.path.join(dir_checkm, 'bins/test1_mock19.bifido/genes.fna'),
            }, {
                'id': 'test2_mock3.bifido',
                'fai': 'tests/data/test1_mock19.bifido.fasta.gz',
                'fao': os.path.join(self.odir, 'test2_mock3.bifido.fasta'),
                'hmm': os.path.join(dir_checkm, 'bins/test2_mock3.bifido/hmmer.analyze.txt'),
                'fna': os.path.join(dir_checkm, 'bins/test2_mock3.bifido/genes.fna'),
            }, {
                'id': 'test4_mock10.acnes',
                'fai': 'tests/data/test4_mock10.acnes.fasta.gz',
                'fao': os.path.join(self.odir, 'test4_mock10.acnes.fasta'),
                'hmm': os.path.join(dir_checkm, 'bins/test4_mock10.acnes/hmmer.analyze.txt'),
                'fna': os.path.join(dir_checkm, 'bins/test4_mock10.acnes/genes.fna'),
            }
        ]

        self.sag1 = build_sag(test_data[0], marker_set, self.odir)
        self.sag2 = build_sag(test_data[1], marker_set, self.odir)
        self.sag3 = build_sag(test_data[2], marker_set, self.odir)

    def test_calc_ani(self):
        odir = os.path.join(self.odir, 'ani')
        if os.path.isdir(odir):
            shutil.rmtree(odir)
        os.makedirs(odir, exist_ok=True)

        pair1 = PairwiseSag(self.sag1, self.sag2)
        pair2 = PairwiseSag(self.sag1, self.sag3)
        pair1.calc_ani(odir)
        pair2.calc_ani(odir)

        self.assertTrue(os.path.isfile(os.path.join(odir, 'test1_mock19.bifido.fasta_test2_mock3.bifido.fasta')))
        self.assertTrue(os.path.isfile(os.path.join(odir, 'test1_mock19.bifido.fasta_test4_mock10.acnes.fasta')))
        self.assertEqual(pair1.ani, 100.0)
        self.assertEqual(pair2.ani, 0)

    def test_calc_jaccard(self):
        odir = os.path.join(self.odir, 'blastn')
        if os.path.isdir(odir):
            shutil.rmtree(odir)
        os.makedirs(odir, exist_ok=True)

        pair1 = PairwiseSag(self.sag1, self.sag2)
        pair2 = PairwiseSag(self.sag1, self.sag3)
        pair1.calc_jaccard(odir)
        pair2.calc_jaccard(odir)

        self.assertTrue(os.path.isfile(os.path.join(odir, 'test1_mock19.bifido.fasta_test2_mock3.bifido.fasta')))
        self.assertTrue(os.path.isfile(os.path.join(odir, 'test1_mock19.bifido.fasta_test4_mock10.acnes.fasta')))
        self.assertEqual(pair1.jaccard, 1.0)
        self.assertEqual(pair2.jaccard, 0.77068)

    def test_calc_tnf(self):
        pair1 = PairwiseSag(self.sag1, self.sag2)
        pair2 = PairwiseSag(self.sag1, self.sag3)
        pair1.calc_tnf()
        pair2.calc_tnf()
        
        self.assertEqual(round(pair1.tnf_cor, 4), 1.0)
        self.assertEqual(round(pair2.tnf_cor, 4), 0.7294)
