from unittest import TestCase
from ccsag.libccsag1 import CompositeSag, PairwiseSag, SingleCopyGene, Tnf, Sag
import gzip
import os
import shutil

def build_sag(rec, marker_set, odir):
    sag = Sag(rec['id'])
    with gzip.open(rec['fai'], mode="rb") as fh:
        with open(rec['fao'], mode="wb") as fo:
            shutil.copyfileobj(fh, fo)
            sag.fasta = rec['fao']

    spg = SingleCopyGene()
    ofna = os.path.join(odir, '{}.marker.fna'.format(rec['id']))
    spg.extract(rec['hmm'], rec['fna'], marker_set[rec['id']], ofna)
    sag.scg_markers = spg.markers
    sag.scg_attr = spg.attr
    sag.scg_fna = ofna

    tnf = Tnf(odir, rec['fao'])
    tnf.calc()
    sag.tnf_pat = tnf.get_pattern()
    sag.tnf_freq = tnf.get_values()

    return sag

class TestCompositeSag(TestCase):
    def test_identify_cluster(self):
        odir = 'tests/output/composite_sag'
        odir_ani = os.path.join(odir, 'ani')
        odir_jaccard = os.path.join(odir, 'jaccard')
        os.makedirs(odir_ani, exist_ok=True)
        os.makedirs(odir_jaccard, exist_ok=True)

        dir_checkm = 'tests/data/checkm'
        bin_stats = os.path.join(dir_checkm, 'storage/bin_stats_ext.tsv')
        marker_set = {}
        with open(bin_stats) as fh:
            for line in fh:
                cols = line.rstrip().split('\t')
                marker_set[cols[0]] = eval(cols[1])

        test_data = [
            {
                'id': 'test1_mock19.bifido',
                'fai': 'tests/data/test1_mock19.bifido.fasta.gz',
                'fao': os.path.join(odir, 'test1_mock19.bifido.fasta'),
                'hmm': os.path.join(dir_checkm, 'bins/test1_mock19.bifido/hmmer.analyze.txt'),
                'fna': os.path.join(dir_checkm, 'bins/test1_mock19.bifido/genes.fna'),
            }, {
                'id': 'test2_mock3.bifido',
                'fai': 'tests/data/test1_mock19.bifido.fasta.gz',
                'fao': os.path.join(odir, 'test2_mock3.bifido.fasta'),
                'hmm': os.path.join(dir_checkm, 'bins/test2_mock3.bifido/hmmer.analyze.txt'),
                'fna': os.path.join(dir_checkm, 'bins/test2_mock3.bifido/genes.fna'),
            }, {
                'id': 'test4_mock10.acnes',
                'fai': 'tests/data/test4_mock10.acnes.fasta.gz',
                'fao': os.path.join(odir, 'test4_mock10.acnes.fasta'),
                'hmm': os.path.join(dir_checkm, 'bins/test4_mock10.acnes/hmmer.analyze.txt'),
                'fna': os.path.join(dir_checkm, 'bins/test4_mock10.acnes/genes.fna'),
            }, {
                'id': 'test5_mock25.acnes',
                'fai': 'tests/data/test5_mock25.acnes.fasta.gz',
                'fao': os.path.join(odir, 'test5_mock25.acnes.fasta'),
                'hmm': os.path.join(dir_checkm, 'bins/test5_mock25.acnes/hmmer.analyze.txt'),
                'fna': os.path.join(dir_checkm, 'bins/test5_mock25.acnes/genes.fna'),
            }
        ]

        sags = list(map(lambda v: build_sag(v, marker_set, odir), test_data))
        n = len(sags)
        pairs = []
        for i in range(0, n - 1):
            for j in range(i + 1, n):
                pair = PairwiseSag(sags[i], sags[j])
                pair.calc_jaccard(odir_jaccard)
                pair.calc_ani(odir_ani)
                pair.calc_tnf()
                pairs.append(pair)

        for pair in pairs:
            pair.is_same_strain = pair.ani >= 95.0 and pair.jaccard >= 0.99 and pair.tnf_cor >= 0.9
        same_pairs = list(filter(lambda v: v.is_same_strain, pairs))

        composite_id = 1
        paired_sags = list(set(map(lambda v: v.sag1, same_pairs)) | set(map(lambda v: v.sag2, same_pairs)))
        paired_sags = sorted(paired_sags, key=lambda s: s.sag_id)
        for sag in paired_sags:
            if sag.composite_id is None:
                CompositeSag.identify_cluster(sag, same_pairs, composite_id, 'Test')
                composite_id += 1
        
        self.assertEqual(len(paired_sags), 4)
        self.assertEqual(paired_sags[0].sag_id, 'test1_mock19.bifido')
        self.assertEqual(paired_sags[1].sag_id, 'test2_mock3.bifido')
        self.assertEqual(paired_sags[2].sag_id, 'test4_mock10.acnes')
        self.assertEqual(paired_sags[3].sag_id, 'test5_mock25.acnes')
        self.assertEqual(paired_sags[0].composite_id, 'Test-C00001')
        self.assertEqual(paired_sags[1].composite_id, 'Test-C00001')
        self.assertEqual(paired_sags[2].composite_id, 'Test-C00002')
        self.assertEqual(paired_sags[3].composite_id, 'Test-C00002')

