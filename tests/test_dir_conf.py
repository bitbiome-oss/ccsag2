from unittest import TestCase
from ccsag.libccsag1 import DirConf
import shutil
import os

class TestDirConf(TestCase):
    def test_dir_conf(self):
        odir = 'tests/output/dir_conf'
        if os.path.isdir(odir):
            shutil.rmtree(odir)
        intermediate = 'intermediate'
        subdirs = ['fasta', 'checkm', 'sngl_cp_gn', 'marker_dist', 'fastani', 'tnf']
        dirConf = DirConf(odir, intermediate, subdirs)

        self.assertEqual(dirConf.fasta, os.path.join(odir, intermediate, 'fasta'))
        self.assertEqual(dirConf.checkm, os.path.join(odir, intermediate, 'checkm'))
        self.assertEqual(dirConf.sngl_cp_gn, os.path.join(odir, intermediate, 'sngl_cp_gn'))
        self.assertEqual(dirConf.marker_dist, os.path.join(odir, intermediate, 'marker_dist'))
        self.assertEqual(dirConf.fastani, os.path.join(odir, intermediate, 'fastani'))
        self.assertEqual(dirConf.tnf, os.path.join(odir, intermediate, 'tnf'))
        self.assertTrue(os.path.isdir(dirConf.fasta))
        self.assertTrue(os.path.isdir(dirConf.checkm))
        self.assertTrue(os.path.isdir(dirConf.sngl_cp_gn))
        self.assertTrue(os.path.isdir(dirConf.marker_dist))
        self.assertTrue(os.path.isdir(dirConf.fastani))
        self.assertTrue(os.path.isdir(dirConf.tnf))


