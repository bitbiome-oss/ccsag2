from unittest import TestCase
from ccsag.bruteforce import Jaccard
from ccsag.seq_data_obj import Fasta
import shutil
import os

class TestJaccard(TestCase):
    def test_setWorkingDir(self):
        fas = []
        bf = Jaccard(*fas)
        bf.setWorkingDir('tests/output/bruteforce')
        self.assertEqual(bf.wdir, 'tests/output/bruteforce')

    def test_calcDist(self):
        dir = 'tests/output/bruteforce'
        if os.path.isdir(dir):
            shutil.rmtree(dir)

        fas = [
            Fasta('tests/data/genome1.fasta'),
            Fasta('tests/data/genome2.fasta'),
            Fasta('tests/data/genome3.fasta'),
            Fasta('tests/data/genome4.fasta'),
            Fasta('tests/data/genome5.fasta'),
        ]
        bf = Jaccard(*fas)
        bf.setWorkingDir(dir)
        bf.calcDist()
        dist = bf.getDist()

        self.assertEqual(dist[fas[0]][fas[1]] , 0.8)
        self.assertEqual(dist[fas[0]][fas[2]] , 0.75)
        self.assertEqual(dist[fas[0]][fas[3]] , 0)
        self.assertEqual(dist[fas[0]][fas[4]] , 0)
        self.assertEqual(dist[fas[3]][fas[4]] , 1)
        self.assertTrue(os.path.isdir(dir))
        self.assertTrue(os.path.isfile(os.path.join(dir, 'JaccardDistance.txt')))
        self.assertTrue(os.path.isfile(os.path.join(dir, 'JaccardDistance.tsv')))

    def test_clean(self):
        dir = 'tests/output/bruteforce2'
        if os.path.isdir(dir):
            shutil.rmtree(dir)

        fas = [
            Fasta('tests/data/genome1.fasta'),
            Fasta('tests/data/genome2.fasta'),
        ]
        bf = Jaccard(*fas)
        bf.setWorkingDir(dir)
        bf.calcDist()
        dist = bf.getDist()

        self.assertEqual(dist[fas[0]][fas[1]] , 0.8)
        self.assertTrue(os.path.isfile(os.path.join(dir, 'JaccardDistance.txt')))
        self.assertTrue(os.path.isfile(os.path.join(dir, 'JaccardDistance.tsv')))
        self.assertTrue(os.path.isdir(dir))

        bf.clean()
        self.assertFalse(os.path.isdir(dir))
        
