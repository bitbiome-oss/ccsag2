from unittest import TestCase
from ccsag.clustering import DBSCAN, SetCover, Star
from ccsag.bruteforce import Jaccard
from ccsag.seq_data_obj import Fasta
import shutil
import os

class TestClustering(TestCase):
    def setUp(self):
        self.dir = "tests/output/clustering"
        if os.path.isdir(self.dir):
            shutil.rmtree(self.dir)

        fas = [
            Fasta('tests/data/genome1.fasta'),
            Fasta('tests/data/genome2.fasta'),
            Fasta('tests/data/genome3.fasta'),
            Fasta('tests/data/genome4.fasta'),
            Fasta('tests/data/genome5.fasta'),
        ]
        bf = Jaccard(*fas)
        bf.setWorkingDir(self.dir)
        bf.calcDist()

        self.dist = bf.getDist()

    def test_setWorkingDir(self):
        cls = DBSCAN({})
        cls.setWorkingDir('tests/output/clustering')
        self.assertEqual(cls.wdir, 'tests/output/clustering')

    def test_DBSCAN(self):
        cls = DBSCAN(self.dist)
        cls.setWorkingDir(self.dir)
        cls.clustering()
        cluster = cls.getCluster()

        self.assertEqual(len(cluster.keys()), 2)

    def test_SetCover(self):
        cls = SetCover(self.dist)
        cls.setWorkingDir(self.dir)
        cls.clustering()
        cluster = cls.getCluster()

        self.assertEqual(len(cluster.keys()), 2)

    def test_Star(self):
        cls = Star(self.dist)
        cls.setWorkingDir(self.dir)
        cls.clustering()
        cluster = cls.getCluster()

        self.assertEqual(len(cluster.keys()), 1)
