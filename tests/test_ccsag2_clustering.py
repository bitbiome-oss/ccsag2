from unittest import TestCase
import subprocess
import os
import json
import shutil

class TestCcsag2Clustering(TestCase):
    def setUp(self):
        self.odir = 'tests/output/ccsag2'
        os.makedirs(self.odir, exist_ok=True)
        self.fasta = [
          'tests/data/test1_mock19.bifido.ffn',
          'tests/data/test2_mock3.bifido.ffn',
          'tests/data/test3_mock4.bifido.ffn',
          'tests/data/test4_mock10.acnes.ffn',
          'tests/data/test5_mock25.acnes.ffn',
          'tests/data/test6_mock9.acnes.ffn',
        ]
        for src in self.fasta:
            dst = os.path.join(self.odir, os.path.basename(src))
            subprocess.run('zcat {}.gz > {}'.format(src, dst), shell=True)

    def test_clustering_setcover(self):
        odir = 'tests/output/ccsag2/clustering-setcover'
        if os.path.isdir(odir):
            shutil.rmtree(odir)
        cmd = [
          'python ./ccsag2.py clustering-fast',
          '-o', odir,
          '-w', os.path.join(odir, 'work'),
          'SetCover',
          os.path.join(self.odir, 'test1_mock19.bifido.ffn'),
          os.path.join(self.odir, 'test2_mock3.bifido.ffn'),
          os.path.join(self.odir, 'test3_mock4.bifido.ffn'),
          os.path.join(self.odir, 'test4_mock10.acnes.ffn'),
          os.path.join(self.odir, 'test5_mock25.acnes.ffn'),
          os.path.join(self.odir, 'test6_mock9.acnes.ffn'),
        ]
        subprocess.run(' '.join(cmd), shell=True)

        self.assertTrue(os.path.exists(os.path.join(odir, 'SAG_clustering/SetCover_result.json')))
        self.assertFalse(os.path.isdir(os.path.join(odir, 'work/Clustering')))
        self.assertFalse(os.path.isdir(os.path.join(odir, 'work/Distance')))

        with open(os.path.join(odir, 'SAG_clustering/SetCover_result.json')) as fh:
            js = json.load(fh)
            n = len(js.keys())
            self.assertEqual(n, 2)

    def test_clustering_setcover_keep(self):
        odir = 'tests/output/ccsag2/clustering-setcover-keep'
        if os.path.isdir(odir):
            shutil.rmtree(odir)
        cmd = [
          'python ./ccsag2.py clustering-fast',
          '-o', odir,
          '-w', os.path.join(odir, 'work'),
          '--keep-intermediate',
          'SetCover',
          os.path.join(self.odir, 'test1_mock19.bifido.ffn'),
          os.path.join(self.odir, 'test2_mock3.bifido.ffn'),
          os.path.join(self.odir, 'test3_mock4.bifido.ffn'),
          os.path.join(self.odir, 'test4_mock10.acnes.ffn'),
          os.path.join(self.odir, 'test5_mock25.acnes.ffn'),
          os.path.join(self.odir, 'test6_mock9.acnes.ffn'),
        ]
        subprocess.run(' '.join(cmd), shell=True)

        self.assertTrue(os.path.exists(os.path.join(odir, 'SAG_clustering/SetCover_result.json')))
        self.assertTrue(os.path.isdir(os.path.join(odir, 'work/Clustering')))
        self.assertTrue(os.path.isdir(os.path.join(odir, 'work/Distance')))

        with open(os.path.join(odir, 'SAG_clustering/SetCover_result.json')) as fh:
            js = json.load(fh)
            n = len(js.keys())
            self.assertEqual(n, 2)

    def test_clustering_dbscan(self):
        odir = 'tests/output/ccsag2/clustering-dbscan'
        if os.path.isdir(odir):
            shutil.rmtree(odir)
        cmd = [
          'python ./ccsag2.py clustering-fast',
          '-o', odir,
          '-w', os.path.join(odir, 'work'),
          'DBSCAN',
          os.path.join(self.odir, 'test1_mock19.bifido.ffn'),
          os.path.join(self.odir, 'test2_mock3.bifido.ffn'),
          os.path.join(self.odir, 'test3_mock4.bifido.ffn'),
          os.path.join(self.odir, 'test4_mock10.acnes.ffn'),
          os.path.join(self.odir, 'test5_mock25.acnes.ffn'),
          os.path.join(self.odir, 'test6_mock9.acnes.ffn'),
        ]
        subprocess.run(' '.join(cmd), shell=True)

        self.assertTrue(os.path.exists(os.path.join(odir, 'SAG_clustering/DBSCAN_result.json')))
        self.assertFalse(os.path.isdir(os.path.join(odir, 'work/Clustering')))
        self.assertFalse(os.path.isdir(os.path.join(odir, 'work/Distance')))

        with open(os.path.join(odir, 'SAG_clustering/DBSCAN_result.json')) as fh:
            js = json.load(fh)
            n = len(js.keys())
            self.assertEqual(n, 2)

    def test_clustering_dbscan_keep(self):
        odir = 'tests/output/ccsag2/clustering-dbscan-keep'
        if os.path.isdir(odir):
            shutil.rmtree(odir)
        cmd = [
          'python ./ccsag2.py clustering-fast',
          '-o', odir,
          '-w', os.path.join(odir, 'work'),
          '--keep-intermediate',
          'DBSCAN',
          os.path.join(self.odir, 'test1_mock19.bifido.ffn'),
          os.path.join(self.odir, 'test2_mock3.bifido.ffn'),
          os.path.join(self.odir, 'test3_mock4.bifido.ffn'),
          os.path.join(self.odir, 'test4_mock10.acnes.ffn'),
          os.path.join(self.odir, 'test5_mock25.acnes.ffn'),
          os.path.join(self.odir, 'test6_mock9.acnes.ffn'),
        ]
        subprocess.run(' '.join(cmd), shell=True)

        self.assertTrue(os.path.exists(os.path.join(odir, 'SAG_clustering/DBSCAN_result.json')))
        self.assertTrue(os.path.isdir(os.path.join(odir, 'work/Clustering')))
        self.assertTrue(os.path.isdir(os.path.join(odir, 'work/Distance')))

        with open(os.path.join(odir, 'SAG_clustering/DBSCAN_result.json')) as fh:
            js = json.load(fh)
            n = len(js.keys())
            self.assertEqual(n, 2)

    def test_clustering_star(self):
        odir = 'tests/output/ccsag2/clustering-star'
        if os.path.isdir(odir):
            shutil.rmtree(odir)
        cmd = [
          'python ./ccsag2.py clustering-fast',
          '-o', odir,
          '-w', os.path.join(odir, 'work'),
          'Star',
          os.path.join(self.odir, 'test1_mock19.bifido.ffn'),
          os.path.join(self.odir, 'test2_mock3.bifido.ffn'),
          os.path.join(self.odir, 'test3_mock4.bifido.ffn'),
          os.path.join(self.odir, 'test4_mock10.acnes.ffn'),
          os.path.join(self.odir, 'test5_mock25.acnes.ffn'),
          os.path.join(self.odir, 'test6_mock9.acnes.ffn'),
        ]
        subprocess.run(' '.join(cmd), shell=True)

        self.assertTrue(os.path.exists(os.path.join(odir, 'SAG_clustering/Star_result.json')))
        self.assertFalse(os.path.isdir(os.path.join(odir, 'work/Clustering')))
        self.assertFalse(os.path.isdir(os.path.join(odir, 'work/Distance')))

        with open(os.path.join(odir, 'SAG_clustering/Star_result.json')) as fh:
            js = json.load(fh)
            n = len(js.keys())
            self.assertEqual(n, 1)

    def test_clustering_star_keep(self):
        odir = 'tests/output/ccsag2/clustering-star'
        if os.path.isdir(odir):
            shutil.rmtree(odir)
        cmd = [
          'python ./ccsag2.py clustering-fast',
          '-o', odir,
          '-w', os.path.join(odir, 'work'),
          '--keep-intermediate',
          'Star',
          os.path.join(self.odir, 'test1_mock19.bifido.ffn'),
          os.path.join(self.odir, 'test2_mock3.bifido.ffn'),
          os.path.join(self.odir, 'test3_mock4.bifido.ffn'),
          os.path.join(self.odir, 'test4_mock10.acnes.ffn'),
          os.path.join(self.odir, 'test5_mock25.acnes.ffn'),
          os.path.join(self.odir, 'test6_mock9.acnes.ffn'),
        ]
        subprocess.run(' '.join(cmd), shell=True)

        self.assertTrue(os.path.exists(os.path.join(odir, 'SAG_clustering/Star_result.json')))
        self.assertTrue(os.path.isdir(os.path.join(odir, 'work/Clustering')))
        self.assertTrue(os.path.isdir(os.path.join(odir, 'work/Distance')))

        with open(os.path.join(odir, 'SAG_clustering/Star_result.json')) as fh:
            js = json.load(fh)
            n = len(js.keys())
            self.assertEqual(n, 1)
        
    def test_clustering_all(self):
        odir = 'tests/output/ccsag2/clustering-all'
        if os.path.isdir(odir):
            shutil.rmtree(odir)
        cmd = [
          'python ./ccsag2.py clustering-fast',
          '-o', odir,
          '-w', os.path.join(odir, 'work'),
          'all',
          os.path.join(self.odir, 'test1_mock19.bifido.ffn'),
          os.path.join(self.odir, 'test2_mock3.bifido.ffn'),
          os.path.join(self.odir, 'test3_mock4.bifido.ffn'),
          os.path.join(self.odir, 'test4_mock10.acnes.ffn'),
          os.path.join(self.odir, 'test5_mock25.acnes.ffn'),
          os.path.join(self.odir, 'test6_mock9.acnes.ffn'),
        ]
        subprocess.run(' '.join(cmd), shell=True)

        self.assertTrue(os.path.exists(os.path.join(odir, 'SAG_clustering/Star_result.json')))
        self.assertTrue(os.path.exists(os.path.join(odir, 'SAG_clustering/DBSCAN_result.json')))
        self.assertTrue(os.path.exists(os.path.join(odir, 'SAG_clustering/SetCover_result.json')))
        self.assertFalse(os.path.isdir(os.path.join(odir, 'work/Clustering')))
        self.assertFalse(os.path.isdir(os.path.join(odir, 'work/Distance')))

        with open(os.path.join(odir, 'SAG_clustering/SetCover_result.json')) as fh:
            js = json.load(fh)
            n = len(js.keys())
            self.assertEqual(n, 2)
        with open(os.path.join(odir, 'SAG_clustering/DBSCAN_result.json')) as fh:
            js = json.load(fh)
            n = len(js.keys())
            self.assertEqual(n, 2)
        with open(os.path.join(odir, 'SAG_clustering/Star_result.json')) as fh:
            js = json.load(fh)
            n = len(js.keys())
            self.assertEqual(n, 1)
        
    def test_clustering_all_keep(self):
        odir = 'tests/output/ccsag2/clustering-all-keep'
        if os.path.isdir(odir):
            shutil.rmtree(odir)
        cmd = [
          'python ./ccsag2.py clustering-fast',
          '-o', odir,
          '-w', os.path.join(odir, 'work'),
          '--keep-intermediate',
          'all',
          os.path.join(self.odir, 'test1_mock19.bifido.ffn'),
          os.path.join(self.odir, 'test2_mock3.bifido.ffn'),
          os.path.join(self.odir, 'test3_mock4.bifido.ffn'),
          os.path.join(self.odir, 'test4_mock10.acnes.ffn'),
          os.path.join(self.odir, 'test5_mock25.acnes.ffn'),
          os.path.join(self.odir, 'test6_mock9.acnes.ffn'),
        ]
        subprocess.run(' '.join(cmd), shell=True)

        self.assertTrue(os.path.exists(os.path.join(odir, 'SAG_clustering/Star_result.json')))
        self.assertTrue(os.path.exists(os.path.join(odir, 'SAG_clustering/DBSCAN_result.json')))
        self.assertTrue(os.path.exists(os.path.join(odir, 'SAG_clustering/SetCover_result.json')))
        self.assertTrue(os.path.isdir(os.path.join(odir, 'work/Clustering')))
        self.assertTrue(os.path.isdir(os.path.join(odir, 'work/Distance')))

        with open(os.path.join(odir, 'SAG_clustering/SetCover_result.json')) as fh:
            js = json.load(fh)
            n = len(js.keys())
            self.assertEqual(n, 2)
        with open(os.path.join(odir, 'SAG_clustering/DBSCAN_result.json')) as fh:
            js = json.load(fh)
            n = len(js.keys())
            self.assertEqual(n, 2)
        with open(os.path.join(odir, 'SAG_clustering/Star_result.json')) as fh:
            js = json.load(fh)
            n = len(js.keys())
            self.assertEqual(n, 1)
        


