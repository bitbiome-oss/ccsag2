from unittest import TestCase
from ccsag.libccsag1 import Tnf
import gzip
import os
import shutil

class TestTnf(TestCase):
    def setUp(self):
        self.odir = 'tests/output/tnf'
        os.makedirs(self.odir, exist_ok=True)

        self.gz = 'tests/data/test1_mock19.bifido.fasta.gz'
        self.fasta = os.path.join(self.odir, 'test1_mock19.bifido.fasta')
        with gzip.open(self.gz, mode="rb") as fh:
            with open(self.fasta, mode="wb") as fo:
                shutil.copyfileobj(fh, fo)

    def test_calc(self):
        tnf = Tnf(self.odir, self.fasta)
        tnf.calc()

        self.assertTrue(os.path.join(self.odir, 'test1_mock19.bifido.fasta.jf'))

    def test_get_values(self):
        tnf = Tnf(self.odir, self.fasta)
        tnf.calc()
        vals = tnf.get_values()

        self.assertEqual(len(vals), 256)
        self.assertEqual(vals[0], 4178)
        self.assertEqual(vals[255], 4085)

    def test_get_pattern(self):
        tnf = Tnf(self.odir, self.fasta)
        tnf.calc()
        pats = tnf.get_pattern()

        self.assertEqual(len(pats), 256)
        self.assertEqual(pats[0], 'AAAA')
        self.assertEqual(pats[255], 'TTTT')

