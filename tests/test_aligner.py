from unittest import TestCase
from ccsag.aligner import Minimap2
from ccsag.seq_data_obj import Fastq, Fasta
import os
import shutil

class TestAligner(TestCase):
    def setUp(self):
        self.fas = [
            Fasta('tests/data/test1_mock19.bifido.fasta.gz'),
            Fasta('tests/data/test2_mock3.bifido.fasta.gz'),
        ]

    def test_setWorkingDir(self):
        aln = Minimap2(self.fas)
        aln.setWorkingDir('tests/output/align')

        self.assertEqual(aln.wdir, 'tests/output/align')

    def test_createindex(self):
        odir = 'tests/output/align1'
        if os.path.isdir(odir):
            shutil.rmtree(odir)
        aln = Minimap2(self.fas)
        aln.setWorkingDir(odir)
        aln.createindex()

        self.assertTrue(os.path.exists(os.path.join(odir, 'genomes.mmi')))

    def test_mapping(self):
        odir = 'tests/output/align2'
        if os.path.isdir(odir):
            shutil.rmtree(odir)
        fq = Fastq('tests/data/test3_mock4.bifido_1.fastq.gz', 'tests/data/test3_mock4.bifido_2.fastq.gz')
        aln = Minimap2(self.fas)
        aln.setWorkingDir(odir)
        aln.createindex()
        aln.mapping(fq)

        self.assertTrue(os.path.exists(os.path.join(odir, 'genomes.mmi')))
        self.assertTrue(os.path.exists(os.path.join(odir, 'test3_mock4.bifido_1_merged.bam')))

    def test_clean(self):
        odir = 'tests/output/align3'
        if os.path.isdir(odir):
            shutil.rmtree(odir)
        self.assertTrue(True)
        aln = Minimap2(self.fas)
        aln.setWorkingDir(odir)
        aln.createindex()

        self.assertTrue(os.path.exists(os.path.join(odir, 'genomes.mmi')))

        aln.clean()
        self.assertFalse(os.path.isdir(aln.wdir))

