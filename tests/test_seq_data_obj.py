from unittest import TestCase
from ccsag.seq_data_obj import Fasta, Fastq, Bam
import gzip
import os

class TestFasta(TestCase):
    def test_getSequences(self):
        fa = Fasta('tests/data/test7.fna')
        self.assertEqual(fa.path, 'tests/data/test7.fna')
        self.assertEqual(fa.name, 'test7')

        seq_list = fa.getSequences()
        self.assertEqual(seq_list[1], 'ATGCCGTCTAACGAAAAAAAAGCATTAATTACAGAATCCTTTATTGGATACAAAAAAATAGCACCGTCTT')
        self.assertEqual(len(seq_list), 2)

class TestFastq(TestCase):
    def test_single_end(self):
        fq = Fastq('tests/data/test7_1.fastq.gz')
        self.assertEqual(fq.name, 'test7_1')
        self.assertEqual(fq.forward, 'tests/data/test7_1.fastq.gz')
        self.assertIsNone(fq.reverse)
        self.assertFalse(fq.isPaired)

    def test_paired_end(self):
        fq = Fastq('tests/data/test7_1.fastq.gz', 'tests/data/test7_2.fastq.gz')
        self.assertTrue(fq.isPaired)
        self.assertEqual(fq.forward, 'tests/data/test7_1.fastq.gz')
        self.assertEqual(fq.reverse, 'tests/data/test7_2.fastq.gz')

class TestBam(TestCase):
    def test_rmChimera(self):
        bam = Bam('tests/data/test7.bam')
        self.assertEqual(bam.bam, "tests/data/test7.bam")

        fq = 'tests/output/test7.rm_chimera.fastq.gz'
        bam.rmChimera(fq)
        self.assertTrue(os.path.isfile(fq))

        seqs = []
        with gzip.open(fq, mode='rt') as fh:
            for i, line in enumerate(fh):
                if i % 4 == 1:
                    seqs.append(line.strip())
        self.assertEqual(len(seqs), 5)
        self.assertEqual(seqs[0], 'GTAATCCCGCCTTTTCGAGCTCGAAAACCGCATACTAAAAACGTTTTTTCTC')
        self.assertEqual(seqs[1], 'AAAAAAAGTATGCGCGGTGTATCAAGCCCGAAAATTACGGGGACGAATCGTTTTTTCTTGGATTTACCCGTAATCGCGCCCGAAGCCATCCCACCCTGA')
        self.assertEqual(seqs[2], 'AAAAAAAGTATGCGCGGTGTATCAAGCCCGAAAATTACGGGGACGAATCGTTTT')
        self.assertEqual(seqs[3], 'CAAGCCCGAAAATTACGGGGACGAATCGTTTTTTCTTGGATTTACCCGTAATCGCGCCCGAAGCCATCCCACCCTGACAGCCGCAGAGCAGCAAAAAGGATCGCCGAAGCGACCCTTTCAATCAGCAGTTATCAATACGGCAATATCCTCG')
        self.assertEqual(seqs[4], 'GAGCCAATAAGCAGCGCCGTCAGCATCGGCTTCGCGGTTCATGAATGTTGAGTACAAACGCGCAAGGAATTCCGCGTCAGACAGCTTGAATCCGATCATCTCATCGGACAGGAAGAAAGCCGCGCCGACTTGCT')
        os.remove(fq)

    def test_rmChimera_len10(self):
        bam = Bam('tests/data/test7.bam')
        self.assertEqual(bam.bam, "tests/data/test7.bam")

        fq = 'tests/output/test7.rm_chimera.len10.fastq.gz'
        bam.rmChimera(fq, 10)
        self.assertTrue(os.path.isfile(fq))

        seqs = []
        with gzip.open(fq, mode='rt') as fh:
            for i, line in enumerate(fh):
                if i % 4 == 1:
                    seqs.append(line.strip())
        self.assertEqual(len(seqs), 6)
        self.assertEqual(seqs[5], 'AGATCGGAGAGCACACG')

        os.remove(fq)

