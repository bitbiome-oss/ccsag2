from unittest import TestCase
from ccsag.libccsag1 import Threshold
import shutil
import os

class TestThreshold(TestCase):
    def test_threshold(self):
        th = Threshold({'comp': 20, 'cont': 10, 'jaccard': 0.99, 'ani': 95, 'tnf_cor': 0.9, 'msize': 1})

        self.assertEqual(th.comp, 20)
        self.assertEqual(th.cont, 10)
        self.assertEqual(th.jaccard, 0.99)
        self.assertEqual(th.ani, 95)
        self.assertEqual(th.tnf_cor, 0.9)
        self.assertEqual(th.msize, 1)
