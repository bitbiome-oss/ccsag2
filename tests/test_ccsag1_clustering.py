from unittest import TestCase
import subprocess
import os
import json
import shutil
import csv

def read_result(path):
    data = []
    with open(path) as fh:
        reader = csv.DictReader(fh, delimiter="\t")
        for row in reader:
            data.append(row)
    return data

class TestCcsag1Clustering(TestCase):
    def setUp(self):
        self.odir = 'tests/output/ccsag1'
        os.makedirs(self.odir, exist_ok=True)
        self.fasta = [
          'tests/data/test1_mock19.bifido.fasta',
          'tests/data/test2_mock3.bifido.fasta',
          'tests/data/test3_mock4.bifido.fasta',
          'tests/data/test4_mock10.acnes.fasta',
          'tests/data/test5_mock25.acnes.fasta',
          'tests/data/test6_mock9.acnes.fasta',
        ]
        for src in self.fasta:
            dst = os.path.join(self.odir, os.path.basename(src))
            subprocess.run('zcat {}.gz > {}'.format(src, dst), shell=True)

    def test_clustering(self):
        odir = os.path.join(self.odir, 'clustering')
        if os.path.isdir(odir):
            shutil.rmtree(odir)

        cmd = [
          'python ./ccsag2.py clustering-careful',
          '-o', odir,
          '-w', 'work',
          os.path.join(self.odir, 'test1_mock19.bifido.fasta'),
          os.path.join(self.odir, 'test2_mock3.bifido.fasta'),
          os.path.join(self.odir, 'test3_mock4.bifido.fasta'),
          os.path.join(self.odir, 'test4_mock10.acnes.fasta'),
          os.path.join(self.odir, 'test5_mock25.acnes.fasta'),
          os.path.join(self.odir, 'test6_mock9.acnes.fasta'),
        ]
        subprocess.run(' '.join(cmd), shell=True)

        fout = os.path.join(odir, 'ccsag1_clustering/clustering_result.txt')
        wdir = os.path.join(odir, 'ccsag1_clustering/work')
        data = read_result(fout)
        cls1 = list(filter(lambda v: v['csag_id'] == 'Strain-C00001', data))
        cls2 = list(filter(lambda v: v['csag_id'] == 'Strain-C00002', data))
        self.assertTrue(os.path.isfile(fout))
        self.assertTrue(os.path.isfile(os.path.join(wdir, 'pairwise_sag.txt')))
        self.assertEqual(len(data), 6)
        self.assertEqual(len(cls1), 3)
        self.assertEqual(len(cls2), 3)
        self.assertFalse(os.path.isdir(os.path.join(wdir, 'checkm')))
        self.assertFalse(os.path.isdir(os.path.join(wdir, 'fasta')))
        self.assertFalse(os.path.isdir(os.path.join(wdir, 'fastani')))
        self.assertFalse(os.path.isdir(os.path.join(wdir, 'marker_dist')))
        self.assertFalse(os.path.isdir(os.path.join(wdir, 'sngl_cp_gn')))
        self.assertFalse(os.path.isdir(os.path.join(wdir, 'tnf')))

    def test_clustering_keep(self):
        odir = os.path.join(self.odir, 'clustering-keep')
        if os.path.isdir(odir):
            shutil.rmtree(odir)
        os.makedirs(self.odir, exist_ok=True)
        for src in self.fasta:
            dst = os.path.join(odir, os.path.basename(src))
            subprocess.run('zcat {}.gz > {}'.format(src, dst), shell=True)

        cmd = [
          'python ./ccsag2.py clustering-careful',
          '-o', odir,
          '-w', 'work',
          '--keep-intermediate',
          os.path.join(self.odir, 'test1_mock19.bifido.fasta'),
          os.path.join(self.odir, 'test2_mock3.bifido.fasta'),
          os.path.join(self.odir, 'test3_mock4.bifido.fasta'),
          os.path.join(self.odir, 'test4_mock10.acnes.fasta'),
          os.path.join(self.odir, 'test5_mock25.acnes.fasta'),
          os.path.join(self.odir, 'test6_mock9.acnes.fasta'),
        ]
        subprocess.run(' '.join(cmd), shell=True)

        fout = os.path.join(odir, 'ccsag1_clustering/clustering_result.txt')
        wdir = os.path.join(odir, 'ccsag1_clustering/work')
        data = read_result(fout)
        cls1 = list(filter(lambda v: v['csag_id'] == 'Strain-C00001', data))
        cls2 = list(filter(lambda v: v['csag_id'] == 'Strain-C00002', data))
        self.assertTrue(os.path.isfile(fout))
        self.assertTrue(os.path.isfile(os.path.join(wdir, 'pairwise_sag.txt')))
        self.assertEqual(len(data), 6)
        self.assertEqual(len(cls1), 3)
        self.assertEqual(len(cls2), 3)
        self.assertTrue(os.path.isdir(os.path.join(wdir, 'checkm')))
        self.assertTrue(os.path.isdir(os.path.join(wdir, 'fasta')))
        self.assertTrue(os.path.isdir(os.path.join(wdir, 'fastani')))
        self.assertTrue(os.path.isdir(os.path.join(wdir, 'marker_dist')))
        self.assertTrue(os.path.isdir(os.path.join(wdir, 'sngl_cp_gn')))
        self.assertTrue(os.path.isdir(os.path.join(wdir, 'tnf')))

